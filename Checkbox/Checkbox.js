import React from 'react';
import PropTypes from 'prop-types';
import styles from './Checkbox.module.scss';

const Checkbox = ({label, errorMessage, refs, ...props}) => {
  delete props.label;

  const checkMarkStyles = [styles.checkMark];
  if (errorMessage) {
    checkMarkStyles.push(styles.hasError);
  }
  return (
    <label className={styles.checkboxGroup}>
      <div className={styles.label}>{label}
        {errorMessage && <div className="text-xs text-red">{errorMessage}</div>}
      </div>
      <input
        type="checkbox"
        {...refs}
        {...props}
      />
      <span className={checkMarkStyles.join(" ")}/>
    </label>
  );
};

Checkbox.propTypes = {};

export default Checkbox;

Checkbox.propTypes = {
  label: PropTypes.any.isRequired,
  hasError: PropTypes.bool,
  refs: PropTypes.object
}