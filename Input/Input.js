import React, {useRef, useState} from 'react';
import PropTypes from 'prop-types';
import styles from './Input.module.scss';
import {ExclamationCircleIcon} from "react-line-awesome";

const Input = ({
                 isTextarea = false,
                 icon,
                 iconClick,
                 currentValue,
                 errorMessage,
                 iconPosition = 'left',
                 noBorder,
                 setValue,
                 setFocus,
                 selectOptions = [],
                 refs,
                 ...props
               }) => {

  const isSelect = selectOptions.length > 0;

  const [optionsVisible, setOptionsVisible] = useState(false);

  const focusHandler = () => {
    setOptionsVisible(true);
  }

  const blurHandler = () => {
    setTimeout(() => {
      setOptionsVisible(false);
    }, 200);
  }


  const selectHandler = (label) => {
    setValue(refs.name, label);
  }

  const inputStyles = [
    styles.input,
    styles[iconPosition],
    props.className,
  ];
  const inputWrapperStyles = [
    styles.inputWrapper
  ];

  if (noBorder) {
    inputStyles.push(styles.noBorder);
  }
  if (isTextarea) {
    inputStyles.push(styles.textarea);
  }
  if (currentValue || props.defaultValue) {
    inputStyles.push(styles.hasValue);
  }
  if (errorMessage) {
    inputStyles.push(styles.hasError);
  }
  if (isSelect) {
    props.readOnly = true;
  }
  if (optionsVisible) {
    inputWrapperStyles.push(styles.selectShow);
  }

  delete props.className;
  delete props.currentValue;
  delete props.errorMessage;

  const placeholderLabel = props.placeholder

  delete props.placeholder;

  const iconClickHandler = () => {
    if (iconClick === undefined) {
      setFocus(refs.name)
    } else {
      iconClick();
    }
  }

  let iconJsx = null;
  if (icon) {
    iconJsx = <span onClick={iconClickHandler} className={styles.inputIcon}>{icon}</span>
  }

  let optionsJsx = null;
  if (isSelect) {
    optionsJsx = (
      <div className={styles.options}>
        {selectOptions.map(option => {
          return (
            <div
              key={option.value}
              onClick={() => selectHandler(option.label)}
              className={styles.option}
            >
              {option.label}
            </div>
          );
        })}
      </div>
    );
  }

  if (errorMessage) {
    iconClick = undefined;
    iconJsx = (
      <span
        onClick={iconClickHandler}
        className={styles.inputIcon}>
        <ExclamationCircleIcon className="text-2xl"/>
      </span>
    );
  }

  return (
    <div className={inputWrapperStyles.join(" ")}>
      {placeholderLabel && isTextarea && <label htmlFor={props.id}
                                                onClick={() => setFocus(refs.name)}
                                                className={styles.placeholderTextArea}>{placeholderLabel}</label>}
      <div className={styles.inputGroup} onBlur={blurHandler}>
        {isTextarea
          ? <textarea onFocus={focusHandler} className={inputStyles.join(" ")} {...refs} {...props} />
          : <input onFocus={focusHandler} className={inputStyles.join(" ")} {...refs} {...props} />
        }

        {iconJsx}
        {placeholderLabel && !isTextarea
          &&
          <label
            htmlFor={props.id}
            onClick={() => setFocus(refs.name)}
            className={styles.placeholder}>
            {placeholderLabel}
          </label>
        }
      </div>
      {optionsJsx}
      {errorMessage && <div className="text-xs text-red">{errorMessage}</div>}
    </div>
  );
};

Input.propTypes = {
  isTextarea: PropTypes.bool,
  selectOptions: PropTypes.arrayOf((propValue, key, componentName) => {

  }),
  errorMessage: PropTypes.string,
  icon: PropTypes.any,
  setValue: PropTypes.func,
  iconPosition: PropTypes.oneOf(['left', 'right', '']),
  noBorder: PropTypes.bool,
  iconClick: PropTypes.func,
  currentValue: PropTypes.string,
  refs: PropTypes.object
};

export default Input;