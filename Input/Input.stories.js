import React from "react";
import Input from "./Input";
import {useForm} from "react-hook-form";
import {CarAltIcon} from "react-line-awesome";

export default {
  title: 'Input',
  component: Input,
};


const Template = (args) => {
  const {register, watch, setValue, setFocus} = useForm();

  const placeholderValue = watch("placeholder");

  return (
    <Input {...args}
           setValue={setValue}
           setFocus={setFocus}
           currentValue={placeholderValue}
           defaultValue={placeholderValue}
           refs={register("placeholder", {required: true})}
    />
  );
}


export const Standard = Template.bind({});
Standard.args = {
  placeholder: "placeholder text",
};

export const Textarea = Template.bind({});
Textarea.args = {
  placeholder: "placeholder Textarea",
  isTextarea: true
};

export const StandardWithIcon = Template.bind({});
StandardWithIcon.args = {
  placeholder: "placeholder text withIcon",
  icon: <CarAltIcon className="text-2xl"/>
};

export const Error = Template.bind({});
Error.args = {
  placeholder: "placeholder text",
  errorMessage: "Errore, il placeholder è sbagliato!"
};

export const Select = Template.bind({});
Select.args = {
  placeholder: "placeholder text",
  name: "placeholder",
  selectOptions: [
    {
      value: 1,
      label: "Option 1"
    },
    {
      value: 2,
      label: "Option 2"
    }
  ],
};
