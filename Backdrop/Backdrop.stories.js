import React from 'react';
import Backdrop from "./Backdrop";

export default {
  title: 'Backdrop',
  component: Backdrop
}

const Template = args => <Backdrop {...args} />

export const Standard = Template.bind({});
Standard.args = {
  onClick : () => {},
};