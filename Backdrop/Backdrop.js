import React from 'react';
import PropTypes from 'prop-types';
import styles from './Backdrop.module.scss';


const Backdrop = ({style, onClick, ...props}) => {

  const backdropStyles = [styles.Backdrop, props.className];
  delete props.className;

  return (
    <div style={style} className={backdropStyles.join(" ")} onClick={onClick}/>
  );
};
export default Backdrop;

Backdrop.propTypes = {
  style: PropTypes.object,
  onClick: PropTypes.func
}