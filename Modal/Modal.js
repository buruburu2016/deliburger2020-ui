import React, {useEffect} from 'react';
import {useRouter} from "next/router";
import {Transition} from "react-transition-group";
import PropTypes from 'prop-types';
import Backdrop from "../Backdrop/Backdrop";
import styles from "./Modal.module.scss";


const Modal = ({
                 children,
                 hash,
                 direction,
                 className,
                 classBackdrop,
                 closeModal,
                 hasBackdrop,
                 isShow,
                 timeout = 400
               }) => {

  const modalStyles = [styles.modal, styles[direction], className];

  const router = useRouter();


  const enterHandler = () => {
  }
  const enteredHandler = () => {
    document.body.style.overflow = 'hidden';
    if (hash) {
      if (!router.asPath.includes("#" + hash)) {
        router.push(router.asPath + '#' + hash);
      }
    }
  }

  const exitHandler = () => {
    document.body.style.overflow = 'auto';
  }
  const exitedHandler = () => {
    if (hash) {
      const newRoute = router.asPath.replace("#" + hash, "");
      router.push(newRoute, undefined, {shallow: true});
    }
  }


  const handleHashChange = (url, {shallow}) => {
    if (hash) {
      if (!window.location.hash && !url.includes("#" + hash)) {
        // console.log(!window.location.hash, !url.includes("#" + hash), url);
        // console.log(closeModal);
        closeModal();
      }
    }
  };

  useEffect(() => {
    return () => {
      document.body.style.overflow = 'auto';
    }
  }, [])

  useEffect(() => {
    if (hash) {
      if (isShow) {
        router.events.on("hashChangeComplete", handleHashChange);
      }
      return () => {
        router.events.off('hashChangeComplete', handleHashChange)
      }
    }
  }, [isShow])

  switch (direction) {
    case 'fade':
      modalStyles.push(styles.fade)
      break;
    case 'top' :
      modalStyles.push(styles.top);
      break;
    case 'bottom' :
      modalStyles.push(styles.bottom);
      break;
    case 'right' :
      modalStyles.push(styles.right);
      break;
    case 'left' :
      modalStyles.push(styles.left);
      break;
    default:
      modalStyles.push(styles.top);
  }

  return (
    <Transition onEntering={enterHandler} onEntered={enteredHandler} onExit={exitHandler} onExited={exitedHandler}
                in={isShow} timeout={timeout}
                mountOnEnter
                unmountOnExit>
      {(state) => {
        return (
          <>
            <div className={styles.modalDialog}>
              {hasBackdrop
                ? <Backdrop
                  className={classBackdrop}
                  style={{
                    transition: "opacity 0.3s",
                    opacity: state === "entered" ? 1 : 0,
                  }}
                  onClick={closeModal}/>
                : null}
              <div
                className={modalStyles.join(" ") + " " + styles[state]}
              >
                {children}
              </div>
            </div>
          </>
        );
      }}
    </Transition>
  );
};


Modal.propTypes = {
  children: PropTypes.any,
  hash: PropTypes.string,
  closeModal: PropTypes.func,
  hasBackdrop: PropTypes.bool,
  direction: PropTypes.oneOf(['fade', 'top', 'bottom', 'right', 'left']),
  isShow: PropTypes.bool,
  timeout: PropTypes.number
};

export default Modal;