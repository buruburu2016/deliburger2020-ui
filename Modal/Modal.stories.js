import React from 'react';
import Modal from "./Modal";

export default {
  title: 'Modal',
  component: Modal
}

const Template = args => <Modal {...args} />

export const Standard = Template.bind({});
Standard.args = {
  children: <p>inside the modal</p>,
  hasBackdrop: false,
  translateWidth: 900,
  isShow: false,
  timeout: 400,
  onClick : () => {},
};
export const WithBackdrop = Template.bind({});
WithBackdrop.args = {
  children: <p>inside the modal with Backdrop</p>,
  hasBackdrop: true,
  translateWidth: 900,
  isShow: true,
  timeout: 400,
  onClick : () => {},
};