import React from 'react';
import PropTypes from 'prop-types';
import styles from './Panel.module.scss';


const Panel = ({header, children, variant, footer}) => {

  const panelStyles = [styles.panel, styles[variant]];

  return (
    <div className={panelStyles.join(" ")}>
      {header
        ?
        <div className={styles.header}>
          {header}
        </div>
        : null}
      {children}
    </div>
  );
};

const Body = ({children, variant}) => {
  const panelBodyStyles = [styles.body, styles[variant]];
  return (
    <div className={panelBodyStyles.join(" ")}>{children}</div>
  );
}

Panel.Body = Body;

export default Panel;

Panel.propTypes = {
  variant: PropTypes.oneOf(['primary', 'grey', 'account', '']),
  header: PropTypes.any,
  children: PropTypes.any.isRequired
}