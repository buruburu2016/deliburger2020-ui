import React from 'react';

import Panel from "./Panel";
import Button from "../Button/Button";


export default {
  title: 'Panel',
  component: Panel,
};

const Template = (args) => <Panel {...args} />;

export const NoHeader = Template.bind({});
NoHeader.args = {
  children: <Button variant="primary" size="xl">Panel Button</Button>
};

export const PanelDoubleBody = Template.bind({});
PanelDoubleBody.args = {
  children: (<>
    <Panel.Body><Button variant="thin" size="lg">Panel Button</Button></Panel.Body>
    <Panel.Body><Button variant="thin" size="lg">Adjacent Panel Button</Button></Panel.Body>
  </>)
}

export const WithHeader = Template.bind({});
WithHeader.args = {
  header: <>Header Text Jsx</>,
  children: <Button variant="primary" size="xl">Panel Button</Button>
};
