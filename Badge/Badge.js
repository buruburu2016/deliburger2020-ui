import React from 'react';
import Link from "next/link";
import PropTypes from 'prop-types';
import styles from './Badge.module.scss';

const Badge = ({variant, children, size, ...props}) => {
	const badgeStyles = [
		props.className,
		styles.badge,
		styles[variant],
		styles[size],
	];

	delete props.className;

	return (
		<div className={badgeStyles.join(" ")} {...props} >
			{children}
		</div>
	);
};

export default Badge;

Badge.propTypes = {
	variant: PropTypes.oneOf(['active', 'inactive']),
	/**
	 * How large should the button be?
	 */
	size: PropTypes.oneOf(['xs', 'sm', 'md', '', 'lg', 'xl', 'full']),
	/**
	 * Button contents
	 */
	children: PropTypes.any,
};

Badge.defaultProps = {
	size: '',
};
