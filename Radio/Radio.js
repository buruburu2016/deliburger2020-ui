import React from 'react';
import styles from "./Radio.module.scss";


const Radio = ({label, errorMessage, refs, ...props}) => {
  delete props.label;
  const checkRadioStyles = [styles.checkRadio];
  if (errorMessage) {
    checkRadioStyles.push(styles.hasError);
  }
  return (
    <label className={styles.radioGroup}>
      <input
        type="radio"
        {...refs}
        {...props}
      />
      <span className={checkRadioStyles.join(" ")}/>
      <div className={styles.label}>
        {label}
        {errorMessage && <div className="text-xs text-red">{errorMessage}</div>}
      </div>
    </label>
  );
};

export default Radio;