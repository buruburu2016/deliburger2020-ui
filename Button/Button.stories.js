import React from 'react';
import Button from "./Button";

export default {
  title: 'Button',
  component: Button,
};

const Template = (args) => <Button {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  variant: '',
  children: 'Button',
};
export const Primary = Template.bind({});
Primary.args = {
  variant: 'primary',
  children: 'Button',
};

export const Inactive = Template.bind({});
Inactive.args = {
  children: 'Button',
  variant: 'inactive'
};

export const Large = Template.bind({});
Large.args = {
  size: 'xl',
  variant: 'primary',
  children: 'Button',
};

export const Small = Template.bind({});
Small.args = {
  size: 'xs',
  children: 'Button',
};
