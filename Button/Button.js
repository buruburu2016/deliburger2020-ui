import React from 'react';
import Link from "next/link";
import PropTypes from 'prop-types';
import styles from './Button.module.scss';


/**
 * Primary UI component for user interaction
 */
const Button = ({variant, children, size, to, icon, noTransition = false, ...props}) => {

  const buttonStyles = [
    props.className,
    styles.button,
    styles[variant],
    styles[size]
  ];

  if (icon) {
    buttonStyles.push(styles.withIcon)
  }
  if (noTransition){
    buttonStyles.push(styles.noTransition);
  }

  delete props.className;

  return (

    to ?
      <Link href={to}>
        <a className={buttonStyles.join(" ")} {...props}>
          {icon}
          {children}
        </a>
      </Link>
      :
      <button className={buttonStyles.join(" ")} {...props} >
        {icon}
        {children}
      </button>

  );
};

export default Button;

Button.propTypes = {
  noTransition: PropTypes.bool,
  variant: PropTypes.oneOf(['', "facebook", 'primary', 'primary-outline', 'inactive', 'thin', 'disabled']),

  to: PropTypes.string,
  icon: PropTypes.element,
  /**
   * How large should the button be?
   */
  size: PropTypes.oneOf(['xs', 'sm', 'md', '', 'lg', 'xl', 'full', 'rounded', 'splash']),

  /**
   * Button contents
   */
  children: PropTypes.any,
  /**
   * Optional click handler
   */
  onClick: PropTypes.func,
};

Button.defaultProps = {
  size: '',
  onClick: undefined,
};
