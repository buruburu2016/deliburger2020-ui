import * as React from "react";

const SvgExclamationTriangleSolid = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" {...props}>
    <path d="m16 3.219-.875 1.5-12 20.781-.844 1.5H29.72l-.844-1.5-12-20.781Zm0 4L26.25 25H5.75ZM15 14v6h2v-6Zm0 7v2h2v-2Z" />
  </svg>
);

export default SvgExclamationTriangleSolid;
