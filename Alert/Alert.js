import React from 'react';
import PropTypes from 'prop-types';
import styles from "./Alert.module.scss";
import ExclamationCircleSolid from "./icons/ExclamationCircleSolid";
import CheckCircle from "./icons/CheckCircle";
import ExclamationTriangleSolid from "./icons/ExclamationTriangleSolid";

const Alert = ({variant = "info", featured, children}) => {

  const variantStyles = [styles.alert, styles[variant]];

  let icon;

  switch (variant) {
    case "info":
      icon = <ExclamationCircleSolid className={styles.icon}/>
      break;
    case "success":
      icon = <CheckCircle className={styles.icon}/>
      break;
    case "danger":
      icon = <ExclamationCircleSolid className={styles.icon}/>
      break;
    case "warning":
      icon = <ExclamationTriangleSolid className={styles.icon}/>
      break;
    default:
      icon = <ExclamationCircleSolid className={styles.icon}/>
  }

  return (
    <div className={variantStyles.join(" ")}>
      {icon}
      <span>
        <div className={styles.featured}>{featured}</div>
        {children}
      </span>
    </div>
  );
};

Alert.propTypes = {
  variant: PropTypes.oneOf(["success", "info", "danger", "warning"]),
  featured: PropTypes.string
};

export default Alert;