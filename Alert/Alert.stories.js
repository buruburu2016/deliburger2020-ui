import React from 'react';
import Alert from "./Alert";

export default {
  title: 'Alert',
  component: Alert,
  argTypes: {
    variant: {
      options: ["info", "warning", "danger", "success"],
      control: {type: 'radio'},
    },
    children: {
      control: {type: "text"}
    }
  }
};

const Template = (args) => <Alert {...args} />;

export const Info = Template.bind({});
Info.args = {
  variant: 'info',
  featured: "Nota bene",
  children: 'Info message - check it out!',
};

export const Danger = Template.bind({});
Danger.args = {
  variant: 'danger',
  featured: "Nota bene",
  children: 'Danger message - check it out!',
};

export const Warning = Template.bind({});
Warning.args = {
  variant: 'warning',
  featured: "Nota bene",
  children: 'Warning message - check it out!',
};

export const Success = Template.bind({});
Success.args = {
  variant: 'success',
  featured: "Nota bene",
  children: 'Success message - check it out!',
};