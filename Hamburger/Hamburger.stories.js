import React from 'react';
import Hamburger from "./Hamburger";

export default {
  title: 'Hamburger',
  component: Hamburger
}

const Template = (args) => <Hamburger {...args} />

export const Standard = Template.bind({});
Standard.args = {

};
export const HamburgerGrey = Template.bind({});
HamburgerGrey.args = {
  variant: 'grey'
};