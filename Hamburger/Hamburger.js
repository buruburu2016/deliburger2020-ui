import React from 'react';
import PropTypes from 'prop-types';
import styles from './Hamburger.module.scss';

const Hamburger = ({variant, isOpen, onClick}) => {
  const hamburgerStyles = [styles.hamburger];

  hamburgerStyles.push(styles[variant]);
  if (isOpen){
    hamburgerStyles.push(styles.open);
  }

  return (
    <div onClick={onClick} className={hamburgerStyles.join(" ")}>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default Hamburger;

Hamburger.propTypes = {
  variant: PropTypes.oneOf(['grey', '']),
  isOpen: PropTypes.bool,
  onClick: PropTypes.func

}